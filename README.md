# [IDP LOGBOOK](https://gitlab.com/nrlzulkifli/idp_airship_development.git)

# INTRODUCTION

## *안녕하세요!*

<img src="/Myself.jpg" height="300" width="230">


👋 Hi! My name is Nurul Ain Izzati and you can called me Nurul or Ain

### Details About Me

- 22 Years Old
- I'm from Kuala Terengganu, Terengganu
- Lived in Batu Caves, Selangor
- My hobby are crafting and also reading
- I Love cats ~~but I'm allergic to some of them~~

***Strenght & Weakness***
| Strength | Weakness |
| ------ | ------ |
| Optimism | Balance |
| Creative | Sensitive |
| Communication | Overthink |
| Honesty | Insecure |

>I'm a fan of Sir Lewis Hamilton hihi :grin:
